#!/bin/bash
#SBATCH -J debra_mtss
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --partition=snowball
#SBATCH -t 71:59:59


## load modules
module load r/4.3.1

## run script
Rscript "13-MTSS-Boostrap-Prediction-Accuracy.R"

