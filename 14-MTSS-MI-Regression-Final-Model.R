# DEBRA: Regression models using multiple imputations  ---------------------------------------------
#
# Author: AR
# Date:   2023-07-07
#
# packages -----------------------------------------------------------------------------------------
library(openxlsx)
library(dplyr)
library(Hmisc)
library(DT)
library(mice)
library(nnet)
library(tidyr)
library(broom)
library(broom.mixed)
library(splines)
library(emmeans)
library(ggplot2)
library(ggsci)
library(ggeffects)
library(data.table)

# GRIDs --------------------------------------------------------------------------------------------
# grid of functional forms
rcs_grid <- expand.grid(c("time", "ns(time, df = 2)", "ns(time, df = 3)"),
                        c("ns(alter, df = 2)", "ns(alter, df = 4)"),
                        c("ns(incomeOECD, df = 2)", "ns(incomeOECD, df = 4)"))

# all interactions selected from variable selection frequencies
intx <- c("sex:region2",
          "education:region2",
          "sex:education",
          "alter:incomeOECD")

# each of these variables can be selected (1) or not (0)
ps <- rep(list(0:1), length(intx))

# create all possible combinations
intx_grid <- expand.grid(ps)

# names columns by terms
names(intx_grid) <- intx

# Which model was best? ----------------------------------------------------------------------------
pa <- readRDS("T:/Studydata/DEBRA/RData/mtss-prediction-accuracy-pdi-2023-11-15.RDS")

PDIave <- aggregate(pa$PDI, list(pa$RCS, pa$INTX), mean)
names(PDIave) <- c("RCS", "INTX", "PDI_MEAN")


PDIave <- PDIave %>% 
  arrange(desc(PDI_MEAN))

rcs_grid[] <- sapply(rcs_grid[], as.character)
rcs_grid$Var1 <- gsub("ns\\(time", "T", rcs_grid$Var1)
rcs_grid$Var1 <- gsub("\\)", "", rcs_grid$Var1)
rcs_grid$Var1 <- gsub(" ", "", rcs_grid$Var1)
rcs_grid$Var2 <- gsub("ns\\(alter", "A", rcs_grid$Var2)
rcs_grid$Var2 <- gsub("\\)", "", rcs_grid$Var2)
rcs_grid$Var2 <- gsub(" ", "", rcs_grid$Var2)
rcs_grid$Var3 <- gsub("ns\\(incomeOECD", "I", rcs_grid$Var3)
rcs_grid$Var3 <- gsub("\\)", "", rcs_grid$Var3)
rcs_grid$Var3 <- gsub(" ", "", rcs_grid$Var3)



PDIave$INTX_Text <- NA
PDIave$RCS_Text <- NA

# Add model complexity
for (i in 1:nrow(PDIave)) {
  
  rcs_s  <- PDIave$RCS[i]
  intx_s <- PDIave$INTX[i]   
  intx_abbr <- c("S:R", "E:R", "S:E",  "A:I")
  
  # no interaction
  if (length(names(intx_grid)[as.logical(intx_grid[intx_s, ])]) == 0) {
    intx <- "none"
    ## with interaction
  } else {
    intx <- paste0(intx_abbr[as.logical(intx_grid[intx_s, ])], collapse = " + ")
  }
  
  PDIave$INTX_Text[i] <- intx
  
  PDIave$RCS_Text[i] <- paste0(rcs_grid[rcs_s, ], collapse = "|")
  
}

PDIave$RCS_Text[1:3]

# Plot best/worst models
PDIave$BWModel <- factor(c(1, 2, 3, rep(4, dim(PDIave)[1] - 6), 5, 6, 7), levels = 1:7, 
                         labels = c("A:I \n(T,df=3|A,df=4|I,df=2)",
                                    "S:R + A:I \n(T,df=3|A,df=4|I,df=2)",
                                    "A:I \n(T|A,df=4|I,df=2)",
                                    "Else", 
                                    "E:R+S:E+A:I \n(T,df=2|A,df=2|I,df=4)",
                                    "S:R+E:R+S:E+A:I \n(T|A,df=2|I,df=4)",
                                    "S:R+E:R+S:E+A:I \n(T,df=2|A,df=2|I,df=4)"))

pa <- merge(pa, PDIave, by = c("RCS", "INTX"), all.x = TRUE)

ggplot(pa[pa$BWModel != "Else", ], aes(x = BWModel, y = PDI)) + 
  geom_boxplot() + 
  xlab("PDI of the three best and worst models \nwith respective interaction and functional form") +
  theme_minimal(base_size = 14) 



# study data ---------------------------------------------------------------------------------------
load("T:/Studydata/DEBRA/RData/lds_mi_pp.RData")
load("T:/Studydata/DEBRA/RData/vds_mi_pp.RData")

# user functions
source("C:/Users/Adrian/Documents/Software/R/Programmbeispiele/common-util-functions.R")

# complete case analysis
lds <- readRDS("T:/Studydata/DEBRA/RData/lds.RDS")

lds$mtss <- car::recode(lds$mrs_comb, "1 = 1; 2:3 = 2; 4:7 = 3")
lds$mtss <- factor(lds$mtss, levels = 1:3, labels = c("don't want",
                                                      "unspecific",
                                                      "want"))

# prepare multinomial outcomes ---------------------------------------------------------------------
ldsmi <- complete(ldsmipp, "long", inc = TRUE)
vdsmi <- complete(vdsmipp, "long", inc = TRUE)

# motivation to stop smoking
ldsmi$mtss <- car::recode(ldsmi$mrs_comb, "1 = 1; 2:3 = 2; 4:7 = 3")
vdsmi$mtss <- car::recode(vdsmi$mrs_comb, "1 = 1; 2:3 = 2; 4:7 = 3")
ldsmi$mtss <- factor(ldsmi$mtss, levels = 1:3, labels = c("don't want",
                                                          "unspecific",
                                                          "want"))
vdsmi$mtss <- factor(vdsmi$mtss, levels = 1:3, labels = c("don't want",
                                                          "unspecific",
                                                          "want"))

ldsmipp <- as.mids(ldsmi)
vdsmipp <- as.mids(vdsmi)

# multinomial regression: MTSS Top Model -----------------------------------------------------------

# apply regression to MI data
mnr1 <- with(ldsmipp, 
             multinom(mtss ~ hsi + ns(time, df = 3) + ns(alter, df = 4) + sex + education + 
                        region2 + ns(incomeOECD, df = 2) + ns(alter, df = 4):ns(incomeOECD, df = 2)))

# pool results
mnr1p <- pool(mnr1)

# summary results
sr <- summary(mnr1p, exponentiate = TRUE, conf.int = TRUE)
sr[3:9] <- apply(sr[3:9], 2, round, digits = 2)
sr$CI <- paste0("[", sr$`2.5 %`,"; ", sr$`97.5 %`, "]")
sr

# openxlsx::write.xlsx(sr, 
#                      file = "C:/Users/Adrian/Documents/git_projects/debra_intersectionality/GandT/Results-Multinomial-Regression-of-MTSS-to-Quit-Smoking.xlsx")


# Calculate estimated marginal means ---------------------------------------------------------------
# create mice data in long format
lds_mice <- complete(ldsmipp, "long", include = FALSE)

# split in list
lds_mice_l <- split(lds_mice, lds_mice$.imp)

# calculate models separately in m=10 imputations
m_i <- lapply(lds_mice_l, function(x) {
  multinom(mtss ~ hsi + ns(time, df = 3) + ns(alter, df = 4) + sex + education + 
             region2 + ns(incomeOECD, df = 2) + ns(alter, df = 4):ns(incomeOECD, df = 2), data = x)
})


# calculate estimated marginal means in each imputation (takes very long ....)
vage <- seq(20, 80, 5)
vinc <- seq(0.25, 3.75, 0.25)
m_i_p <- lapply(m_i, function(x) {
  ggemmeans(x, terms = c("alter [vage]", "incomeOECD [vinc]"))
})

save(m_i_p, file = paste0("T:/Studydata/DEBRA/RData/m_i_p-", Sys.Date(), ".RData"))

# pool emms over imputations
p_mi <- pool_predictions(m_i_p)

saveRDS(p_mi, file = paste0("T:/Studydata/DEBRA/RData/mtss_pooled_emmeans-", Sys.Date(), ".RData"))


# Plot predicted probabilities ---------------------------------------------------------------------
p_mi <- readRDS("T:/Studydata/DEBRA/RData/mtss_pooled_emmeans-2023-11-15.RData")

p_mi <- dplyr::rename(p_mi, c("Age" = "x",
                              "IncomeOECD" = "group"))

p_mi$response.level <- factor(p_mi$response.level, 
                          levels = c("don't want", "unspecific", "want"),
                          labels = c("MTSS: don't want", "MTSS: unspecific", "MTSS: want"))

# mp <- mean(p_mi$predicted[p_mi$response.level != "MTSS: don't want"])
# mp <- mean(p_mi$predicted)


cols <- c("#8c510a", "#bf812d", "#dfc27d", "#f6e8c3", "#f5f5f5", "#c7eae5", "#80cdc1", "#35978f")
cbs <- seq(0.05, 0.75, 0.05)


# MTSS <- ggplot(p_mi[p_mi$response.level != "MTSS: don't want", ], 
#                aes(x = factor(Age), y = factor(IncomeOECD), fill = predicted)) +
MTSS <- ggplot(p_mi, 
               aes(x = factor(Age), y = factor(IncomeOECD), fill = predicted)) +
  facet_grid(response.level ~ .) +
  geom_tile() +
  xlab("Age in 5-year intervals") +
  ylab("Income \n(OECD normalized)") +
  geom_text(aes(factor(Age), 
                IncomeOECD, 
                label = format(round(predicted, digits = 2), digits = 2), 
                color = predicted < 0.15)) +
  scale_color_manual(guide = FALSE, values = c("black", "white")) +
  scale_fill_gradientn(colours = cols, breaks = cbs) +
  theme_minimal(base_size = 14) +
  theme(legend.position = "none")
MTSS

ggsave(last_plot(),
       file = "C:/Users/Adrian/Documents/git_projects/debra_intersections/GandT/f-32-EMM-MTSS.png",
       dpi = 600,
       width = 8,
       height = 15)


# multinomial regression: MTSS Top Model + SEX:REGION ----------------------------------------------

# apply regression to MI data
mnr2 <- with(ldsmipp,
             multinom(mtss ~ hsi + ns(time, df = 3) + ns(alter, df = 4) + sex + education + 
                        region2 + ns(incomeOECD, df = 2) + ns(alter, df = 4):ns(incomeOECD, df = 2) +
                        sex:region2))

# pool results
mnr2p <- pool(mnr2)

# summary results
sr2 <- summary(mnr2p, exponentiate = TRUE, conf.int = TRUE)
sr2[3:9] <- apply(sr2[3:9], 2, round, digits = 2)
sr2$CI <- paste0("[", sr2$`2.5 %`,"; ", sr2$`97.5 %`, "]")
sr2

# openxlsx::write.xlsx(sr, 
#                      file = "C:/Users/Adrian/Documents/git_projects/debra_intersectionality/GandT/Results-Multinomial-Regression-of-MTSS-to-Quit-Smoking.xlsx")


# Calculate estimated marginal means 
# calculate models separately in m=10 imputations
m_i <- lapply(lds_mice_l, function(x) {
  multinom(mtss ~ hsi + ns(time, df = 3) + ns(alter, df = 4) + sex + education + 
             region2 + ns(incomeOECD, df = 2) + 
             sex:region2 +
             ns(alter, df = 4):ns(incomeOECD, df = 2), data = x)
})

# calculate estimated marginal means in each imputation (takes very long ....)
vage <- seq(20, 80, 5)
vinc <- seq(0.25, 3.75, 0.25)
m_i_p <- lapply(m_i, function(x) {
  ggemmeans(x, terms = c("alter [vage]", "sex", "region2"))
})

save(m_i_p, file = paste0("T:/Studydata/DEBRA/RData/m_i_p-SpR-", Sys.Date(), ".RData"))

# pool emms over imputations
p_mi <- pool_predictions(m_i_p)

saveRDS(p_mi, file = paste0("T:/Studydata/DEBRA/RData/mtss_pooled_emmeans-SpR-", Sys.Date(), ".RData"))


# Plot predicted probabilities ---------------------------------------------------------------------
p_mi <- readRDS("T:/Studydata/DEBRA/RData/mtss_pooled_emmeans-SpR-2023-11-15.RData")

p_mi <- dplyr::rename(p_mi, c("Age" = "x",
                              "Sex" = "group",
                              "Region" = "facet"))
# p_mi$Education <- factor(p_mi$Education, 
#                           levels = c("low", "middle", "high"),
#                           labels = c("Education: low", "Education: middle", "Education: high"))

p_mi$response.level <- factor(p_mi$response.level, 
                              levels = c("don't want", "unspecific", "want"),
                              labels = c("MTSS: don't want", "MTSS: unspecific", "MTSS: want"))

mp <- mean(p_mi$predicted[p_mi$response.level != "MTSS: don't want"])
mp <- mean(p_mi$predicted)


MTSSSpR <- ggplot(p_mi[p_mi$response.level != "MTSS: don't want", ], 
               aes(x = factor(Age), y = Region, fill = predicted)) +
  facet_grid(Sex ~ response.level) +
  geom_tile() +
  xlab("Age in 5-year intervals") +
  ylab("Region") +
  geom_text(aes(factor(Age), 
                Region, 
                label = format(round(predicted, digits = 2), digits = 2), 
                color = predicted < 0.1)) +
  scale_color_manual(guide = FALSE, values = c("black", "white")) +
  scale_fill_gradientn(colours = cols, breaks = cbs) +
  theme_minimal(base_size = 14) +
  theme(legend.position = "none")
MTSSSpR

ggsave(last_plot(),
       file = "C:/Users/Adrian/Documents/git_projects/debra_intersections/GandT/f-33-EMM-MTSS-SpR.png",
       dpi = 600,
       width = 12,
       height = 6)

# Complete case analysis ---------------------------------------------------------------------------
lds$mtss <- car::recode(lds$mrs_comb, "1 = 1; 2:3 = 2; 4:7 = 3")
lds$mtss <- factor(lds$mtss, levels = 1:3, labels = c("don't want",
                                                      "unspecific",
                                                      "want"))
mnrcc <- multinom(mtss ~ hsi + ns(time, df = 2) + ns(alter, df = 4) + sex + education + 
                    region2 + incomeOECDcat + ns(alter, df = 4):incomeOECDcat, 
                  data = lds)

srcc <- util_multinom_OR_table(mnrcc)
srcc

openxlsx::write.xlsx(srcc, 
                     file = "C:/Users/Adrian/Documents/git_projects/debra_intersectionality/GandT/Results-Multinomial-Regression-of-MTSS-to-Quit-Smoking-Complete-Case.xlsx")


# EOF ----------------------------------------------------------------------------------------------



