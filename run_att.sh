#!/bin/bash
#SBATCH -J debra_att
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --partition=snowball
#SBATCH -t 71:59:59


## load modules
module load r

## run script
Rscript "07-ATT-Boostrap-Variable-Selection-Frequencies.R"

