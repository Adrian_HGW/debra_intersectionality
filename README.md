# Disclosure

All files in this project can be downloaded, shared and being reused. However, the original study data will not be shared, i.e. __no data__ from the DEBRA study is held in this public project. 

However, the data underlying this study are third-party data and are available to researchers on reasonable request by contacting the study team (sabrina.kastaun@med.uni-duesseldorf.de, daniel.kotz@med.uni-duesseldorf.de). All proposals requesting data access will need to specify how it is planned to use the data, and all proposals will need approval of the study team before data release.

# DEBRA: Intersectionality

This project contains R files used for data preparation and analysis to prepare a publication in the __DEBRA project__: *Intersections of sociodemographic and socioeconomic characteristics, and region of residence in relation to motivation and attempt to quit smoking*. The respective [statistical analysis plan](https://osf.io/x4pwd/) has been published under the open science framework.


# Content

R Files:

- __01-data-preparation.R__: comprises all data preprocessing steps which were forwarded by host of study data
- __02-descriptive-statistics.R__: comprises descriptive analyses to create an overview of distributions and to adapt the SAP
- __03-split-learning-and-validation-data.R__: creates two distinct datasets to learn and validate models
- __04-examine-missingness.R__: examine missingness in learning data
- __05-multiple-imputation.R__: create multiple imputations of missing data
- __06-HPC-preparations__: this file only puts learning data into list to be processed on HPC
- __07-ATT-Boostrap-Variable-Selection-Frequencies.R__: this file computes 500 bootstrap samples within each imputation of data to obtain sampling frequencies of parameters
- __08-ATT-Process-HPC-Results.R__: process bootstrap results for attempts to quit smoking
- __09-ATT-Bootstrap-Prediction-Accuracy.R__: process bootstrap results for attempts to quit smoking
- __10-ATT-MI-Regression-Final-Model.R__: calculates pooled estimates for attempts to quit smoking
- __11-MTSS-Bootstrap-Polytomous-Discrimination-Index.R__: due to computational costs five versions of this file exist (A to E). In respective files the PDI is calculated over bootstrap samples of the learning data. The files vary only regarding the considered subsets of covariates.
- __12-MTSS-Process-HPC-Results.R__: process bootstrap results for motivation to stop smoking
- __13-MTSS-Boostrap-Prediction-Accuracy.R__: calculates prediction accuracy over grid of degrees of freedom used for restricted cubic splines
- __14-MTSS-MI-Regression-Final-Model.R__: calculates pooled estimates for MTSS
- __15-ATT-MTSS-Final-Model-validation-in-validation-data.R__: this file compares the final models selected for ATT and MTSS and compares their predictive accuracy in independent validation data

SLURM and BASH files:

- *run_att.sh* is the bash file to run calculations for attempts on HPC
- *run_mtss.sh* is the bash file to run calculations for motivation on HPC
- *att-slurm-1545594.out* contains the processing time
- *mtts-slurm-1577752.out* contains the processing time

